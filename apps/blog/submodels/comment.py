from django.db import models
from ..models import Article


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('Автор', max_length=50)
    text = models.CharField('Текст комментария', max_length=200)
    date_create = models.DateTimeField('Дата создания')

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
