from django.db import models


class Article(models.Model):
    title = models.CharField('Название записи', max_length=200)
    text = models.TextField('Текст записи')
    date_create = models.DateTimeField('Дата публикации')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
