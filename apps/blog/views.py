from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from .models import Article
from django.urls import reverse
import datetime


def index(request):
    latest_article_list = Article.objects.order_by('-date_create')[:5]
    return render(request, 'blog/list.html', {'data': latest_article_list})


def detail(request, article_id):
    try:
        detail_page = Article.objects.get(id=article_id)
    except:
        raise Http404('Страница не найдена')

    comment_list = detail_page.comment_set.order_by('-id')[:10]
    return render(request, 'blog/detail.html', {'data': detail_page, 'comments': comment_list})


def leave_comment(request, article_id):
    try:
        detail_page = Article.objects.get(id=article_id)
    except:
        raise Http404('Страница не найдена')

    detail_page.comment_set.create(author_name=request.POST['name'], text=request.POST['text'], date_create=datetime.datetime.now())
    return HttpResponseRedirect(reverse('blog:detail', args=(detail_page.id,)))
