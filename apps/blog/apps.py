from django.apps import AppConfig

from apps import blog


class BlogConfig(AppConfig):
    name = 'blog'
    verbose_name = 'Блог'
