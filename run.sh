#!/bin/bash
case $1 in
  dev)
    python3 manage.py runserver --settings=djangotest.settings.dev
    ;;
  prod)
    python3 manage.py collectstatic --noinput
    python3 manage.py makemigrations
    python3 manage.py migrate
    python3 manage.py runserver --settings=djangotest.settings.prod
    ;;
esac