import graphene
import graphql_jwt
import apps.blog.schema as blog
import apps.user.schema as users


class Query(blog.QueryArticle, blog.QueryComment, users.Query, graphene.ObjectType):
    pass


class Mutation(users.Mutation, graphene.ObjectType,):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
